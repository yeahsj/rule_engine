package com.rules.front.api.admin;

import com.rules.admin.entity.NoticeEntity;
import com.rules.admin.service.NoticeService;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 系统公告
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-22 14:19:18
 */
@RestController
@RequestMapping("sys/notice")
public class NoticeController {
    @Autowired
    private NoticeService noticeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Result list(@RequestParam Map<String, Object> params){
        PageUtils page = noticeService.queryPage(params);

        return Result.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public Result info(@PathVariable("id") String id){
		NoticeEntity notice = noticeService.getById(id);

        return Result.ok().put("notice", notice);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Result save(@RequestBody NoticeEntity notice){
		noticeService.save(notice);

        return Result.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Result update(@RequestBody NoticeEntity notice){
		noticeService.updateById(notice);

        return Result.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody String[] ids){
		noticeService.removeByIds(Arrays.asList(ids));

        return Result.ok();
    }

}
