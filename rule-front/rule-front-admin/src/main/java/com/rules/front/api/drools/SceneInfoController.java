package com.rules.front.api.drools;

import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Result;
import com.rules.drools.service.SceneInfoService;
import com.rules.drools.vo.req.RulesSceneEntitiyReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 规则引擎使用场景
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@RestController
@RequestMapping("rules/sceneinfo")
public class SceneInfoController {
    @Autowired
    private SceneInfoService sceneInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Result list(@RequestParam Map<String, Object> params){
        PageUtils page = sceneInfoService.queryPage(params);

        return Result.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{sceneId}")
    public Result info(@PathVariable("sceneId") Long sceneId){
        RulesSceneEntitiyReqVo sceneInfo = sceneInfoService.getOne(sceneId);
        return Result.ok().put("sceneInfo", sceneInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Result save(@RequestBody RulesSceneEntitiyReqVo sceneInfo){
        return sceneInfoService.save(sceneInfo);
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Result update(@RequestBody RulesSceneEntitiyReqVo sceneInfo){
		sceneInfoService.updateScene(sceneInfo);
        return Result.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody Long[] sceneIds){
		sceneInfoService.removeByIds(Arrays.asList(sceneIds));

        return Result.ok();
    }
    /**
     * @Author gz
     * @Description 下拉列表
     * @Date 2019/7/15 16:20
     * @Param
     * @return Map<ID,Name></>
     */
    @RequestMapping("/getSceneNameList")
    public Result getSceneNameList(){
      Map<Long,String> map =  sceneInfoService.listSceneName();
        return Result.ok().put("list",map);
    }

}
