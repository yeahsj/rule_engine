package com.rules.front.api.drools;

import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CreateCache;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Result;
import com.rules.drools.entity.ActionInfoEntity;
import com.rules.drools.service.ActionInfoService;
import com.rules.drools.vo.req.ActionInfoReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 规则动作定义信息
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@RestController
@RequestMapping("rules/actioninfo")
public class ActionInfoController {

    @Autowired
    private ActionInfoService actionInfoService;


    @CreateCache(cacheType= CacheType.LOCAL,expire = 3600)
    private Cache<Long, ActionInfoEntity> actionInfoEntityCache;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public Result list(@RequestParam Map<String, Object> params){
        PageUtils page = actionInfoService.queryPage(params);
        return Result.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{actionId}")
    public Result info(@PathVariable("actionId") Long actionId){
        ActionInfoReqVo info = actionInfoService.getOne(actionId);
        return Result.ok().put("actionInfo", info);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public Result save(@RequestBody ActionInfoReqVo actionInfo){
        return actionInfoService.save(actionInfo);
    }

    /**
     * @Author gz
     * @Description 获取动作名称
     * @Date 2019/7/15 15:39
     * @Param
     * @return Map<动作id,动作名称></>
     */
    @RequestMapping("/getActionNameMap")
    public Result getActionNameMap(){
        List<Map<String,Object>> map = actionInfoService.getActionName();
        return Result.ok().put("list",map);
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public Result update(@RequestBody ActionInfoEntity actionInfo){
		actionInfoService.updateById(actionInfo);
        actionInfoEntityCache.remove(actionInfo.getActionId());
        actionInfoEntityCache.put(actionInfo.getActionId(),actionInfo);
        return Result.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody Long[] actionIds){
		actionInfoService.removeByIds(Arrays.asList(actionIds));
        Arrays.stream(actionIds).forEach(p->{ actionInfoEntityCache.remove(p);});
        return Result.ok();
    }

}
