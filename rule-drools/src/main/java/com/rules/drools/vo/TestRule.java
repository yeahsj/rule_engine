package com.rules.drools.vo;

import lombok.Data;

@Data
public class TestRule {
    private String message;
    private Integer amount = 0;
    private Integer score = 10;
}
