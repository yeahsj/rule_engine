package com.rules.drools.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Query;
import com.rules.drools.dao.ActionParamValueInfoDao;
import com.rules.drools.entity.ActionParamValueInfoEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("actionParamValueInfoService")
public class ActionParamValueInfoService extends ServiceImpl<ActionParamValueInfoDao, ActionParamValueInfoEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ActionParamValueInfoEntity> page = this.page(
                new Query<ActionParamValueInfoEntity>().getPage(params),
                new QueryWrapper<ActionParamValueInfoEntity>()
        );

        return new PageUtils(page);
    }


    /**
     * <p>
     * 方法说明: 根据动作参数或动作与规则关系id获取对应的参数信息
     *
     * @param baseRuleActionParamValueInfo 参数
     */
   public List<ActionParamValueInfoEntity> findBaseRuleActionParamValueInfoList(ActionParamValueInfoEntity baseRuleActionParamValueInfo){
       return this.baseMapper.findBaseRuleActionParamValueInfoList(baseRuleActionParamValueInfo);
    }



    /**
     * <p>
     * 方法说明: 根据参数id获取参数value
     *
     * @param paramId 参数id
     */
    public ActionParamValueInfoEntity findRuleParamValueByActionParamId( Long paramId){
        return this.baseMapper.findRuleParamValueByActionParamId(paramId);
    }

    /**
     * 根据动作id更新动作属性值表中的 动作规则关系表主键id 字段
     * @param actionId 动作id
     * @param ruleActionRelId 动作规则关系表主键id
     * @return
     */
    public boolean updateByActionParamId(Long actionId,Long ruleActionRelId){
        return this.baseMapper.updateByActionParamId(actionId,ruleActionRelId);
    }

    public void removeByActionId(Long actionId) {
        LambdaUpdateWrapper<ActionParamValueInfoEntity> updateWrapper = Wrappers.lambdaUpdate();
        updateWrapper.eq(ActionParamValueInfoEntity::getActionParamId,actionId);
    }
}
