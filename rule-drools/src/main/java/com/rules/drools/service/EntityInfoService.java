package com.rules.drools.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.rules.core.exception.CustomException;
import com.rules.core.utils.CollectionUtil;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Query;
import com.rules.core.utils.Result;
import com.rules.drools.dao.EntityInfoDao;
import com.rules.drools.entity.EntityInfoEntity;
import com.rules.drools.entity.EntityItemInfoEntity;
import com.rules.drools.vo.req.RuleEntityItemReqVo;
import com.rules.drools.vo.req.RulesEntityReqVo;
import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author gz
 * @Description 规则实体Service
 * @Date 2019/7/12 13:56
 */
@Service("entityInfoService")
public class EntityInfoService extends ServiceImpl<EntityInfoDao, EntityInfoEntity> {
    @Autowired
    private Mapper dozerMapper;
    @Autowired
    private EntityItemInfoService entityItemInfoService;


    /**
     * <p>
     * 方法说明: 分页获取实体列表
     *
     * @param params 参数
     */
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<RulesEntityReqVo> page = this.baseMapper.pageEntityInfo(new Query<RulesEntityReqVo>().getPage(params));
        List<RulesEntityReqVo> records = page.getRecords();
        if(!CollectionUtil.collectionIsNull(records)){
            for (RulesEntityReqVo vo : records) {
                List<String> collect = vo.getItem().stream().map(e -> e.getItemIdentify()).collect(Collectors.toList());
                String join = StringUtils.join(collect, ',');
                vo.setItemStr(join);
            }
        }
        page.setTotal(page.getRecords().size());
        return new PageUtils(page);
    }


    /**
     * <p>
     * 方法说明: 保存规则实体信息
     *
     * @param reqVo 参数
     */
    @Transactional(rollbackFor = Exception.class)
    public Result saveOrUpdateRuleEntityInfo(RulesEntityReqVo reqVo) {
        try {
            EntityInfoEntity entity = dozerMapper.map(reqVo, EntityInfoEntity.class);
            this.saveOrUpdate(entity);
            // 实体属性
            List<RuleEntityItemReqVo> item = reqVo.getItem();
            if (CollectionUtil.collectionIsNull(item)) {
                return Result.error("字段不能为空");
            }
            if(reqVo.getId()!=null){
                entityItemInfoService.removeByEntityId(reqVo.getId());
            }
            List<EntityItemInfoEntity> collect = item.stream().map(itemVo -> {
                EntityItemInfoEntity map = dozerMapper.map(itemVo, EntityItemInfoEntity.class);
                map.setEntityId(entity.getEntityId());
                return map;
            }).collect(Collectors.toList());
            entityItemInfoService.saveBatch(collect);
        } catch (Exception e) {
            throw new CustomException("保存实体信息失败", e);
        }
        return Result.ok();
    }

    /**
     * @return Map<ID,Name></>
     * @Author gz
     * @Description 实体下拉列表
     * @Date 2019/7/15 15:55
     * @Param
     */
    public List<Map<String, Object>> listEntityName() {
        List<EntityInfoEntity> list = this.list();
        List<Map<String,Object>> arr = Lists.newArrayList();
        if (CollectionUtil.collectionIsNull(list)) {
            return arr;
        }
        for (EntityInfoEntity entity : list) {
            if(entity.getIsEffect() == 1){
                Map<String,Object> map= new HashMap<>(2);
                map.put("id",entity.getEntityId());
                map.put("fieldName",entity.getEntityName());
                arr.add(map);
            }
        }
        return arr;
    }


    /**
     * <p>
     * 方法说明: 获取规则引擎实体信息
     *
     * @param baseRuleEntityInfo 参数
     */
    public List<EntityInfoEntity> findBaseRuleEntityInfoList(EntityInfoEntity baseRuleEntityInfo) {
        return this.baseMapper.findBaseRuleEntityInfoList(baseRuleEntityInfo);
    }

    /**
     * <p>
     * 方法说明: 根据实体id获取实体信息
     *
     * @param id 实体id
     */
    public EntityInfoEntity findBaseRuleEntityInfoById(Long id) {
        return this.baseMapper.findBaseRuleEntityInfoById(id);
    }

    /**
     * <p>
     * 方法说明: 根据实体id获取实体信息(包含实体参数)
     *
     * @param id 实体id
     */
    public RulesEntityReqVo findOne(Long id) {
        return this.baseMapper.getOne(id);
    }


}
