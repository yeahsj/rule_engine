package com.rules.drools.service.impl;

import com.rules.core.utils.JsonSerializeUtil;
import com.rules.drools.service.DroolsActionService;
import com.rules.drools.vo.fact.RuleExecutionObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 描述：
 */
@Slf4j
@Service
@Transactional
public class TestActionImpl implements DroolsActionService {

    //换行符
    private static final String LINE_SEPARATOR = "line.separator";

    /**
     * <p>
     * 方法说明: 引擎执行结束,实现类默认执行方法
     *
     * @param fact   参数
     * @param result 结果集
     */
    @Override
    public void execute(RuleExecutionObject fact, Map<String,Object> result) {

        System.out.println(System.getProperty(LINE_SEPARATOR));
        System.out.println("=================================规则引擎执行结束,返回信息如下:====================================");
        //遍历map信息
        for (Map.Entry<String, Object> entry : result.entrySet()) {
            System.out.println(System.getProperty(LINE_SEPARATOR));
            System.out.println("=====key=====: " + entry.getKey() + " and value: " + entry.getValue());
        }
        Object o = fact.getFactObjectList().get(0);
        System.out.println("返回数据:{}"+ JsonSerializeUtil.toJson(o));

        System.out.println(System.getProperty(LINE_SEPARATOR));
        System.out.println("=================================规则引擎执行结束====================================");
        fact.setGlobalMap(result);
    }

}
