package com.rules.drools.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Query;
import com.rules.drools.dao.ConditionInfoDao;
import com.rules.drools.entity.ConditionInfoEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("conditionInfoService")
public class ConditionInfoService extends ServiceImpl<ConditionInfoDao, ConditionInfoEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ConditionInfoEntity> page = this.page(
                new Query<ConditionInfoEntity>().getPage(params),
                new QueryWrapper<ConditionInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * <p>
     * 方法说明: 根据规则获取规则条件信息
     *
     * @param baseRuleConditionInfo 参数
     */
   public List<ConditionInfoEntity> findBaseRuleConditionInfoList(ConditionInfoEntity baseRuleConditionInfo){
        return this.baseMapper.findBaseRuleConditionInfoList(baseRuleConditionInfo);
    }

    /**
     * <p>
     * 方法说明: 根据规则id获取规则条件信息
     *
     * @param ruleId 规则id
     */
    public List<ConditionInfoEntity> findRuleConditionInfoByRuleId(final Long ruleId){
        return this.baseMapper.findRuleConditionInfoByRuleId(ruleId);
    }

    /**
     * 通过规则id删除条件
     * @param ruleId
     */
    public void removeByRuleId(Long ruleId){
        LambdaUpdateWrapper<ConditionInfoEntity> updateWrapper = Wrappers.lambdaUpdate();
        updateWrapper.eq(ConditionInfoEntity::getRuleId,ruleId);
        this.remove(updateWrapper);
    }

}
