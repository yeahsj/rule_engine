package com.rules.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rules.drools.entity.ActionParamValueInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 动作参数对应的属性值信息表
 *
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:39
 */
@Mapper
public interface ActionParamValueInfoDao extends BaseMapper<ActionParamValueInfoEntity> {

    /**
     * <p>
     * 方法说明: 根据动作参数或动作与规则关系id获取对应的参数信息
     *
     * @param baseRuleActionParamValueInfo 参数
     */
    List<ActionParamValueInfoEntity> findBaseRuleActionParamValueInfoList(ActionParamValueInfoEntity baseRuleActionParamValueInfo);


    /**
     * <p>
     * 方法说明: 根据参数id获取参数value
     *
     * @param paramId 参数id
     */
    ActionParamValueInfoEntity findRuleParamValueByActionParamId(@Param("paramId") Long paramId);

    /**
     * 根据动作id更新动作属性值表中的 动作规则关系表主键id 字段
     * @param actionId 动作id
     * @param ruleActionRelId 动作规则关系表主键id
     * @return
     */
    boolean updateByActionParamId(@Param("actionId") Long actionId, @Param("ruleActionRelId") Long ruleActionRelId);
}
