package com.rules.drools.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rules.drools.entity.VariableEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 规则引擎常用变量
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Mapper
public interface VariableDao extends BaseMapper<VariableEntity> {
	
}
