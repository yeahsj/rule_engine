package com.rules.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 规则属性配置表
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_property_rel")
public class PropertyRelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long ruleProRelId;
	/**
	 * 规则
	 */
	private Long ruleId;
	/**
	 * 规则属性
	 */
	private Long rulePropertyId;
	/**
	 * 规则属性值
	 */
	private String rulePropertyValue;

}
