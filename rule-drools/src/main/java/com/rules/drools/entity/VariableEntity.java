package com.rules.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 规则引擎常用变量
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_variable")
public class VariableEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long variableId;
	/**
	 * 变量名称
	 */
	private String variableName;
	/**
	 * 变量类型（1条件2动作）
	 */
	private Integer variableType;
	/**
	 * 默认值
	 */
	private String defaultValue;
	/**
	 * 数值类型（ 1字符型 2数字型 3 日期型）
	 */
	private Integer valueType;
	/**
	 * 变量值
	 */
	private String variableValue;
	/**
	 * 是否有效
	 */
	private Integer isEffect;
	/**
	 * 创建人
	 */
	private Long creUserId;
	/**
	 * 创建时间
	 */
	private Date creTime;
	/**
	 * 备注
	 */
	private String remark;

}
