package com.rules.drools.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 场景实体关联表
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-25 14:54:38
 */
@Data
@TableName("rule_scene_entity_rel")
public class SceneEntityRelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long sceneEntityRelId;
	/**
	 * 场景
	 */
	private Long sceneId;
	/**
	 * 实体
	 */
	private Long entityId;

}
