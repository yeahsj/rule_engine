package com.rules.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统消息推送
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-05-22 14:19:18
 */
@Data
@TableName("sys_message_push")
public class MessagePushEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@TableId
	private String id;
	/**
	 * 推送名称
	 */
	private String pushName;
	/**
	 * 推送内容
	 */
	private String pushContent;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 创建时间
	 */
	private Date creatTime;
	/**
	 * 修改人
	 */
	private String updater;
	/**
	 * 修改时间
	 */
	private Date updateTime;

}
