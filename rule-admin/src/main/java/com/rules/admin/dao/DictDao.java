package com.rules.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rules.admin.entity.DictEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典表
 * 
 * @author gz
 * @email 360568523@qq.com
 * @date 2019-04-30 19:55:59
 */
@Mapper
public interface DictDao extends BaseMapper<DictEntity> {
	
}
