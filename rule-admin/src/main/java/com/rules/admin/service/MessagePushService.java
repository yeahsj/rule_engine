package com.rules.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rules.admin.dao.MessagePushDao;
import com.rules.admin.entity.MessagePushEntity;
import com.rules.core.utils.PageUtils;
import com.rules.core.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class MessagePushService extends ServiceImpl<MessagePushDao, MessagePushEntity> {


    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MessagePushEntity> page = this.page(
                new Query<MessagePushEntity>().getPage(params),
                new QueryWrapper<MessagePushEntity>()
        );

        return new PageUtils(page);
    }

}
